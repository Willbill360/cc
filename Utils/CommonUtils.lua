version = "1.0.0"

--[[
  Common Utils module

  This module contains functions that are used by other scripts and modules.
  Tested with CC: Tweaked version 1.98.2

  USAGE:
  - local utils = require("commonUtils")

  PASTEBIN: bJWRLgGE
--]]
local commonUtils = {}

-- Opens rednet on any available side
function commonUtils.openRednet()
  local listOfSides = rs.getSides()
  for _,side in ipairs(listOfSides) do
    if peripheral.isPresent(side) and peripheral.getType(side) == "modem" then
      rednet.open(side)
      return side
    end
  end
  
  print("No modem found, cannot open rednet.")
  return nil
end

-- Waits for a modem to be added
function commonUtils.waitForModem()
  print("Waiting for modem...")
  local _, side = os.pullEvent("peripheral")
  while not peripheral.isPresent(side) or peripheral.getType(side) ~= "modem" do
    side = os.pullEvent("peripheral")
  end
end

-- Clears the terminal
function commonUtils.clearTerm()
  term.clear()
  term.setCursorPos(1, 1)
end

-- Prints a message in the center of the terminal
function commonUtils.printCenter(message)
  local x, y = term.getSize()
  local messageLength = string.len(message)
  local xCenter = math.floor(x / 2)
  local yCenter = math.floor(y / 2)
  local xStart = math.floor(xCenter - messageLength / 2)
  local yStart = math.floor(yCenter - 1)
  term.setCursorPos(xStart, yStart)
  print(message)
end

-- Validate a string representing a side of a computer
-- e.g. "left", "right", "top", "bottom", "front", "back"
function commonUtils.isSide(side)
  local sides = rs.getSides()
  for i = 1, #sides do
    if side == sides[i] then
      return true
    end
  end
  return false
end

-- Get a side based on a number
-- e.g. 1 = left, 2 = right, 3 = top, 4 = bottom, 5 = front, 6 = back
function commonUtils.getSideFromNumber(number)
  local sides = rs.getSides()
  for i = 1, #sides do
    if number == i then
      return sides[i]
    end
  end
end

-- Prints the list of sides
function commonUtils.printSideList()
  local sides = rs.getSides()
  for i = 1, #sides do
    print(i .. ". " .. sides[i])
  end
end

-- Check if the table contains a value
function commonUtils.tableContains(tab, value)
  for index, val in ipairs(tab) do
    if val == value then
      return true
    end
  end

  return false
end

-- Copies a table
function commonUtils.copyTable(tab) 
  if type(tab) ~= "table" then 
    error("copyTable received "..type(tab)..", expected table",2)
  end

  local toRet = {};
  for a, b in pairs(tab) do
    toRet[a] = b
  end

  return toRet
end

-- Converts a string separated by a custom character to a table
function commonUtils.splitString(str, sep)
  local sep = sep or "%s"
  local t = {}

  local regex = "([^"..sep.."]+)"
  if sep == " " or sep == "" or sep == nil then
    regex = "([^%s]+)"
  end

  for str in string.gmatch(str, regex) do
    table.insert(t, str)
  end
  return t
end

-- Returns the arguments as a table
function commonUtils.intializeArgs(originalArgs)
  local tArgs = {}

  tArgs = commonUtils.copyTable(originalArgs)
  for i=1, #tArgs do
    tArgs[i] = tArgs[i]:lower()
    tArgs[tArgs[i]] = i
  end

  return tArgs
end

-- Pauses the execution and waits for a key press
function commonUtils.waitForAnyKey()
  print("Press any key to continue...")
  os.pullEvent("key")
end

-- Ask a yes/no question and return the answer
function commonUtils.askYesNo(question)
  question = question .. " [y/n]: "
  print(question)

  local posX, posY = term.getCursorPos()
  local termX, termY = term.getSize()
  if string.len(question) + 2 <= termX then
    term.setCursorPos(string.len(question) + 1, posY - 1)
  end

  local answer = ""
  while answer ~= "y" and answer ~= "n" do
    answer = string.lower(read())
  end

  return answer
end

-- Ask a questions and return the answer
function commonUtils.prompt(question)
  if not string.find(question, ":") then
    question = question .. ": "
  elseif not string.find (question, ": ") then
    question = question .. " "
  end

  print(question)

  local posX, posY = term.getCursorPos()
  local termX, termY = term.getSize()
  if string.len(question) + 1 <= termX then
    term.setCursorPos(string.len(question) + 1, posY - 1)
  end

  return read()
end

-- Ask a question then validate using the provided function and return the answer
function commonUtils.promptValidate(question, validate)
  local answer = commonUtils.prompt(question)
  while not validate(answer) do
    print("Invalid answer, try again")
    answer = commonUtils.prompt(question)
  end

  return answer
end

-- MUST BE AT THE END OF THE FILE --
return commonUtils
