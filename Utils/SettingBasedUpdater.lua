-- Based on eniallator's updater program.
-- It works by comparing a version variable in 2 files; 1 pastebin link and 1 local file.
-- NOTE: it does not compare the contents of the file, it only compares a variable.
-- ================================================================================
-- USAGE:
-- To make a version variable in your file copy and paste the following into the file:
--
-- version = "*insert version here*"
--
-- This has to be in the local file aswell as the pastebin file to download.
-- ================================================================================
--
-- To add your file to be updated when running this program, run "updater -add <fileName> <pastebinCode>"
-- To update the pastebin code of a file, run "updater -update <fileName> <pastebinCode>"
-- To remove your file from being updated, run "updater -remove <fileName>"
-- To see a list of files that are being updated, run "updater -list"
--
-- The file name must correspond to your local file on your computercraft computer and the pastebin link has to correspond to your file in pastebin.
--
-- PASTEBIN: TSs3jm8f

-- Arguments
local args = {...}

-- Table for files/pastebin codes
local filesToUpdate = settings.get("filesToUpdate", {})

-- Function for downloading the files from pastebin, needs HTTP to run
function getFile(fileName, link)
  local file = http.get("http://pastebin.com/raw.php?i=" .. textutils.urlEncode(link))
  
  if file then
    -- Returning contents of the pastebin file
    local out = file.readAll()
    file.close()
    return out
  else
    -- Returning false if the link is invalid
    return false
  end
end

-- Finding the version number
function getVersion(fileContents)
  if fileContents then
    -- Declaring variables aswell as finding where in the fileContents argument is 'version = "'
    local _, numberChars = fileContents:lower():find('version = "')
    local fileVersion = ""
    local char = ""
  
    if numberChars then
      -- Making the version variable by putting every character from 'version = "' to '"'
      while char ~= '"' do
        numberChars = numberChars + 1
        char = fileContents:sub(numberChars, numberChars)
        fileVersion = fileVersion .. char
      end

      fileVersion = fileVersion:sub(1, #fileVersion - 1)
      return fileVersion
    else
      -- If the function didn't find 'version = "' in the fileContents then it returns false
      return false
    end
  else
    return ""
  end
end


function updateAll()
  for file, url in pairs(filesToUpdate) do
    if not fs.isDir(file) then
      -- Getting the pastebin file's contents
      local pastebinContents = getFile(file,url)
      
      if pastebinContents then
        if fs.exists(file) then
          -- Getting the local file's contents
          local localFile = fs.open(file,"r")
          localContents = localFile.readAll()
          localFile.close()
        end
        
        -- Defining version variables for each of the file's contents
        local pastebinVersion = getVersion(pastebinContents)
        local localVersion = getVersion(localContents)
        
        if not pastebinVersion then
          -- Tests if the pastebin code's contents has a version variable or not
          print("[Error  ] the pastebin code for " .. file .. " does not have a version variable")
        elseif not localVersion then
          -- Tests if the local file doesn't have the version variable
          print("[Error  ] " .. file .. " does not have a version variable")
        elseif pastebinVersion == localVersion then
          -- If the pastebin file's version is equal to the local file's version then it does nothing
          print("[Success] " .. file .. " is already the latest version")
        else
          -- If the versions are not the same then it will write over the current local file to update it to the pastebin version
          endFile = fs.open(file,"w")
          endFile.write(pastebinContents)
          endFile.close()
          
          print("[Success] " .. file .. " has been updated to version " .. pastebinVersion)
        end
      else
        print("[Error  ] " .. file .. " has an invalid link")
       end
    else
      print("[Error  ] " .. file .. " is a directory")
    end
  end
end

if args[1] == "--add" then
  if args[2] and args[3] then
    filesToUpdate[args[2]] = args[3]
    settings.set("filesToUpdate", filesToUpdate)
    settings.save(".settings")
    print("Added " .. args[2] .. " to the list of files to update.")
  else
    print("Usage: updater --add <fileName> <pastebinCode>")
  end
elseif args[1] == "--update" then
  if args[2] and args[3] then
    filesToUpdate[args[2]] = args[3]
    settings.set("filesToUpdate", filesToUpdate)
    settings.save(".settings")
    print("Updated " .. args[2] .. " to the pastebin code.")
  else
    print("Usage: updater --update <fileName> <pastebinCode>")
  end
elseif args[1] == "--remove" then
  if args[2] then
    filesToUpdate[args[2]] = nil
    settings.set("filesToUpdate", filesToUpdate)
    settings.save(".settings")
    print("Removed " .. args[2] .. " from the list of files to update.")
  else
    print("Usage: updater --remove <fileName>")
  end
elseif args[1] == "--list" then
  for file, url in pairs(filesToUpdate) do
    print(file .. ": " .. url)
  end
elseif args[1] == "--help" then
  print("Usage: updater [command] [arguments]")
  print("Leaving the arguments empty will update all files.")
  print("Commands:")
  print("--add <fileName> <pastebinCode>")
  print("--update <fileName> <pastebinCode>")
  print("--remove <fileName>")
  print("--list")
else
  updateAll()
end
