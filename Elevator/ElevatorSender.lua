version = "1.1.0"
--[[
  INFOS:
  - The program is designed to work with the ElevatorSender and ElevatorReceiver programs.
  - The program is designed to work with the Updater program.
  - Current version is compatible with ~1.1.0 of the sender program.
  - Tested with CC: Tweaked version 1.98.2

  MODULES:
  - CommonUtils (bJWRLgGE) as "commonUtils"
  
  USAGE:
  - Have a redstone input sent to one side of the computer.
  - Add redstone on one side toward the bottom door.
  - Add redstone on one side toward the top door.
  - Add at least one ElevatorReceiver.
  - Variables should be changed to fit your needs.

  PASTEBIN: jjLJux8D
--]]

-- Customizable variables
local receiverId = settings.get("receiverId", 2)
local protocol = "Elevator"

-- Program
local utils = require("commonUtils")

if not utils.openRednet() then
  return
end
utils.clearTerm()

print("Connected, ID: " .. os.computerID() .. ". Listening...")
while true do
  os.pullEvent("redstone")
  for index, side in pairs(rs.getSides()) do
    if rs.getInput(side) then
      local time = os.time()
      local formattedTime = textutils.formatTime(time, false)
      print("[".. formattedTime .. "] Sending request to elevator receiver")
  
      rednet.send(receiverId, "toggle", protocol)
      break
    end
  end
end
