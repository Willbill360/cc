version = "1.0.0"
--[[
  ELEVATOR INSTALLER

  This program is designed to install the ElevatorSender and ElevatorReceiver programs.
  It is designed to work and setup with the Updater program.
  Tested with CC: Tweaked version 1.98.2

  -- MODULES
  - CommonUtils (bJWRLgGE) as "commonUtils"

  USAGE:
  - Run inside any of the computers and follow instruction on screen

  PASTEBIN: WySUmiRB
--]]

-- [CUSTOMIZABLE] PROGRAMS
local programs = {
  {
    name = "ElevatorSender",
    pastebin = "jjLJux8D",
    fileName = "elevatorSender"
  },
  {
    name = "ElevatorReceiver",
    pastebin = "Hw3266RJ",
    fileName = "elevatorReceiver"
  }
}

-- Global variables
local programToInstall = nil

-- Program
function waitForAnyKey()
  print("Press any key to continue...")
  os.pullEvent("key")
end

-- Checks if commonUtils is installed
if not fs.exists("commonUtils") then
  term.clear()
  term.setCursorPos(1, 1)

  print("CommonUtils module not found.")
  print("Downloading commonUtils...")
  shell.run("pastebin", "get", "bJWRLgGE", "commonUtils")

  waitForAnyKey()
end
local utils = require("commonUtils")

function getProgramName()
  return programs[programToInstall].name
end

function printHeader()
  utils.clearTerm()
  print("Installer v" .. version)
  print("Current ID: " .. os.computerID())
  print("===================================================")
  print("")
end

function printConfigurationHeader()
  utils.clearTerm()
  print("Install and configure " .. getProgramName())
  print("===================================================")
  print("")
end

function validateProgramChoice(programNumber)
  if tonumber(programNumber) == nil then
    print("Invalid program number.")
    return false
  end

  if tonumber(programNumber) <= #programs or tonumber(programNumber) == 0 then
    return true
  else
    print("Invalid program number.")
    return false
  end
end

function chooseProgram()
  print("Choose program to install:")
  for i = 1, #programs do
    print(i .. " - " .. programs[i].name)
  end
  print("")
  print("0 - Exit")
  print("")
  local programNumber = tonumber(utils.promptValidate("Enter choice: ", validateProgramChoice))
  return programNumber
end

function installProgram(program)
  printConfigurationHeader()
  print("Installing " .. getProgramName() .. "...")

  if fs.exists(program.fileName) then
    if utils.askYesNo(getProgramName() .. " already exists, overwrite?") == "y" then
      fs.delete(program.fileName)
    else
      print("")
      print("Installation aborted.")
      return false
    end
  end

  print("Downloading " .. getProgramName() .. "...")
  shell.run("pastebin", "get", program.pastebin, program.fileName)

  print("")
  print(getProgramName() .. " dowloaded.")

  return true
end

function installUpdater()
  printConfigurationHeader()

  print("Installing Updater...")
  if fs.exists("updater") then
    if utils.askYesNo("Updater already exists, overwrite?") == "y" then
      fs.delete("updater")
    else
      print("")
      print("Installation aborted.")
      return
    end
  end
  shell.run("pastebin", "get", "TSs3jm8f", "updater")

  print("")
  print("Updater installed.")
end

function configureUpdater(program)
  printConfigurationHeader()
  print("Configuring Updater...")
  -- Update the commonUtils file
  shell.run("updater", "--add", "commonUtils", "bJWRLgGE")

  shell.run("updater", "--add", program.fileName, program.pastebin)

  print("")
  print("Updater configured.")
end

function createStartup(program)
  printConfigurationHeader()

  print("Creating startup script...")
  local startup = fs.open("startup", "w")
  startup.write("shell.run(\"updater\")")
  startup.write("shell.run(\"".. program.fileName .."\")")
  startup.close()

  print("")
  print("Startup script created.")
end

-- [CUSTOMIZABLE] SETUP FUNCTIONS
function setupSender()
  local receiverId = utils.prompt("Enter the ID of the receiver: ")
  settings.set("receiverId", receiverId)
end

function setupReceiver()
  local togglerIds = {}
  local togglerId = utils.prompt("Enter the IDs of the togglers (seprated by spaces): ")
  togglerIds = utils.splitString(togglerId, " ")
  while togglerId ~= "" do
    togglerId = utils.prompt("Enter the next ID or press enter to finish: ")
    if togglerId ~= "" then
      table.insert(togglerIds, togglerId)
    end
  end
  settings.set("togglerIds", togglerIds)

  local gearBoxSide = utils.prompt("Enter the side of the gearbox: ")
  if not utils.isSide(gearBoxSide) then
    print("Invalid side.")
    return
  end
  settings.set("gearBoxSide", gearBoxSide)

  local doorDownOutputSide = utils.prompt("Enter the side of the bottom door: ")
  if not utils.isSide(gearBoxSide) then
    print("Invalid side.")
    return
  end
  settings.set("doorDownOutputSide", doorDownOutputSide)

  local doorUpOutputSide = utils.prompt("Enter the side of the top door: ")
  if not utils.isSide(gearBoxSide) then
    print("Invalid side.")
    return
  end
  settings.set("doorUpOutputSide", doorUpOutputSide)
end

-- [CUSTOMIZABLE] SETUP FUNCTIONS
function configureProgram(program)
  printConfigurationHeader()

  -- Configure the program using the setup functions
  if program.name == "ElevatorSender" then
    setupSender()
  elseif program.name == "ElevatorReceiver" then
    setupReceiver()
  end

  settings.save(".settings")

  print("")
  print(getProgramName() .. " configured.")
end

-- [CUSTOMIZABLE] REQUIREMENTS
function checkForRequirements(program)
  printConfigurationHeader()
  
  print("Checking for requirements...")
  
  -- Checks and waits for modem<
  print("Checking for modem...")
  if not utils.openRednet() then
    print("")
    utils.waitForModem()
  end
  print("Modem found.")
  rednet.close()
  waitForAnyKey()

  print("")
  print("All requirements met.")
end

-- Main
printHeader()
programToInstall = chooseProgram()
if programToInstall == 0 then
  print("")
  print("Installation aborted.")
  return
end

local selectedProgram = programs[programToInstall]

checkForRequirements(selectedProgram)
waitForAnyKey()

if not installProgram(selectedProgram) then
  return
end
waitForAnyKey()

configureProgram(selectedProgram)
waitForAnyKey()

-- Ask if we want to install the updater if it doesn't exist
if not fs.exists("updater") then
  printConfigurationHeader()
  if utils.askYesNo("Install Updater?") == "y" then
    installUpdater()
    waitForAnyKey()

    configureUpdater(selectedProgram)
    waitForAnyKey()
  end
else
  configureUpdater(selectedProgram)
  waitForAnyKey()
end

-- Create a startup file
printConfigurationHeader()
if utils.askYesNo("Create startup script?") == "y" then
  if fs.exists("startup") then
    fs.delete("startup")
  end

  createStartup(selectedProgram)
  waitForAnyKey()
end

printHeader()
print("Installation of " .. selectedProgram.name .. " finished. The computer will reboot.")
waitForAnyKey()
os.reboot()
