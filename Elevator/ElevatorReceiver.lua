version = "1.1.0"
--[[
  INFOS:
  - The program is designed to work with the ElevatorSender and ElevatorReceiver programs.
  - The program is designed to work with the Updater program.
  - Current version is compatible with ^1.1.0 of the receiver program.
  - Tested with CC: Tweaked version 1.98.2

  MODULES:
  - CommonUtils (bJWRLgGE) as "commonUtils"
  
  USAGE:
  - Place the computer on the side of the reverse gear.
  - Add redstone on one side toward the bottom door.
  - Add redstone on one side toward the top door.
  - Add at least one ElevatorSender.
  - Variables should be changed to fit your needs.

  PASTEBIN: Hw3266RJ
--]]

-- Customizable variables
local togglerIds = settings.get("togglerIds", {})
local gearBoxSide = settings.get("gearBoxSide", "top")
local doorDownOutputSide = settings.get("doorDownOutputSide", "left")
local doorUpOutputSide = settings.get("doorUpOutputSide", "right")
local protocol = "Elevator"

-- Program
local utils = require("commonUtils")

local isUp = false
function toggleElevator()
  if isUp then
    isUp = false
    redstone.setOutput(gearBoxSide, false)
    redstone.setOutput(doorDownOutputSide, true)
    redstone.setOutput(doorUpOutputSide, false)
    print("Elevator going DOWN")
  else
    isUp = true
    redstone.setOutput(gearBoxSide, true)
    redstone.setOutput(doorDownOutputSide, false)
    redstone.setOutput(doorUpOutputSide, true)
    print("Elevator going UP")
  end
end

if not utils.openRednet() then
  return
end
utils.clearTerm()

redstone.setOutput(gearBoxSide, false)
redstone.setOutput(doorDownOutputSide, true)
redstone.setOutput(doorUpOutputSide, false)

print("ElevatorReceiver v" .. version)
print("Connected, ID: " .. os.computerID() .. ". Listening...")
while true do
  local id, msg = rednet.receive(protocol)

  local time = os.time()
  local formattedTime = textutils.formatTime(time, false)

  if utils.tableContains(togglerIds, id) then
    print("[".. formattedTime .. "] Received request from " .. id)
    toggleElevator()
  end
end
