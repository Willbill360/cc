# ComputerCraft Scripts

Most of the usefull computer craft scripts I have done over the years.
Some scripts have configurations to be tweaked to work and others have installers and saved config.

All the script here are also present on pastebin at <https://pastebin.com/u/Willbill360/1/K3aP3X8V> but are git versionnized.

## Script versions

Most script have a `version = "x.x.x"` line. The reason for this line is to versionize the script but also for the updater to work. It may happen that some scripts have the same version line and were updated. This is in part because we would not want the updater to update this particular script in game.
